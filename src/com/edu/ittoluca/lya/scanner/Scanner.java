/*
 * Copyright (C) 2015 Víctor <viktor.santillan@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.edu.ittoluca.lya.scanner;

import com.edu.ittoluca.lya.archivos.ManejaArchivo;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Víctor <viktor.santillan@gmail.com>
 */
public class Scanner
{

    private Hashtable<String, Integer> palabrasReservadas;
    private ManejaArchivo archivo;
    private String[][] tablaTransiciones;
    StringBuffer buffer;
    private int i;

    public Scanner()
    {
        archivo = new ManejaArchivo();
        tablaTransiciones = archivo.copiarAMatrix();
        palabrasReservadas = new Hashtable<>();
        palabrasReservadas.put("begin", 1);
        palabrasReservadas.put("end", 2);
        palabrasReservadas.put("alias", 3);
        palabrasReservadas.put("and", 4);
        palabrasReservadas.put("or", 4);
        palabrasReservadas.put("break", 5);
        palabrasReservadas.put("case", 6);
        palabrasReservadas.put("class", 7);
        palabrasReservadas.put("def", 8);
        palabrasReservadas.put("do", 9);
        palabrasReservadas.put("else", 10);
        palabrasReservadas.put("false", 11);
        palabrasReservadas.put("true", 11);
        palabrasReservadas.put("for", 12);
        palabrasReservadas.put("if", 13);
        palabrasReservadas.put("in", 14);
        palabrasReservadas.put("next", 15);
        palabrasReservadas.put("nil", 16);
        palabrasReservadas.put("not", 17);
        palabrasReservadas.put("redo", 18);
        palabrasReservadas.put("rescue", 19);
        palabrasReservadas.put("retry", 20);
        palabrasReservadas.put("return", 21);
        palabrasReservadas.put("self", 22);
        palabrasReservadas.put("then", 23);
        palabrasReservadas.put("unless", 24);
        palabrasReservadas.put("until", 25);
        palabrasReservadas.put("when", 26);
        palabrasReservadas.put("while", 27);
        palabrasReservadas.put("it", 28);
        palabrasReservadas.put("puts", 29);
        palabrasReservadas.put("int", 33);
        palabrasReservadas.put("float", 33);
        palabrasReservadas.put("string", 33);

    }

    public void obtenerSiguienteToken()
    {
        int numfila = 1;
        List<String> codigoFuente = archivo.getCodigo();
        for (String linea : codigoFuente)
        {
            for (i = 0; i < linea.length(); i++)
            {
                analizaEntrada(linea, linea.charAt(i), numfila, i);
            }
            numfila++;
        }

    }

    private void analizaEntrada(String linea, char letra, int numLinea, int indice)
    {
        char aux = letra;
        while (esEspacioEnBlanco(aux))
        {
            aux = linea.charAt(indice + 1);
            i++;
            indice++;
        }

        switch (aux)
        {
            case '#':
                System.out.println(linea.substring(indice, linea.length()) + "---> Comentario ---> " + numLinea);
                i = linea.length() - 1;
                break;
            case '(':
                System.out.println(aux + "---> Paréntesis Izquierdo ---> " + numLinea);
                break;
            case ')':
                System.out.println(aux + "---> Paréntesis Derecho ---> " + numLinea);
                break;
            case '{':
                System.out.println(aux + "---> Llave Izquierda ---> " + numLinea);
                break;
            case '}':
                System.out.println(aux + "---> Llave Derecha ---> " + numLinea);
                break;
            case '[':
                System.out.println(aux + "---> Corchete Izquierdo ---> " + numLinea);
                break;
            case ']':
                System.out.println(aux + "---> Corchete Derecho ---> " + numLinea);
                break;
            case ';':
                System.out.println(aux + "---> Punto y Coma ---> " + numLinea);
                break;
            case ',':
                System.out.println(aux + "---> Coma ---> " + numLinea);
                break;
            case '+':
                if (linea.length() - 1 == indice)
                {
                    System.out.println(aux + "---> Operador Aritmético ---> " + numLinea + " 30");
                } else
                {
                    if (linea.charAt(indice + 1) == '+')
                    {
                        System.out.println("++" + "---> Incremento ---> " + numLinea + " 35");
                        i++;
                    } else if (linea.charAt(indice + 1) == '=')
                    {
                        System.out.println("+=" + "---> Operador de Asignación ---> " + numLinea + " 32");
                        i++;
                    } else
                    {
                        System.out.println(aux + "---> Operador Aritmético ---> " + numLinea + 30);
                    }
                }
                break;
            case '-':
                if (linea.length() - 1 == indice)
                {
                    System.out.println(aux + "---> Operador Aritmético ---> " + numLinea + " 30");
                } else
                {
                    if (linea.charAt(indice + 1) == '-')
                    {
                        System.out.println("--" + "---> Decremento ---> " + numLinea + " 35");
                        i++;
                    } else if (linea.charAt(indice + 1) == '=')
                    {
                        System.out.println("-=" + "---> Operador de Asignación ---> " + numLinea + " 32");
                        i++;
                    } else
                    {
                        System.out.println(aux + "---> Operador Aritmético ---> " + numLinea + " 30");
                    }
                }
                break;
            case '/':
                if (linea.length() - 1 == indice)
                {
                    System.out.println(letra + "---> Operador Aritmético ---> " + numLinea + " 30");
                } else
                {
                    if (linea.charAt(indice + 1) == '=')
                    {
                        System.out.println("/=" + "---> Operador de Asignación ---> " + numLinea + " 32");
                        i++;
                    } else
                    {
                        System.out.println(letra + "---> Operador Aritmético ---> " + numLinea + " 30");
                    }
                }
                break;
            case '*':
                if (linea.length() - 1 == indice)
                {
                    System.out.println(letra + "---> Operador Aritmético ---> " + numLinea + " 30");
                } else
                {
                    if (linea.charAt(indice + 1) == '=')
                    {
                        System.out.println("*=" + "---> Operador de Asignación ---> " + numLinea + " 32");
                        i++;
                    } else if (linea.charAt(indice + 1) == '*')
                    {
                        System.out.println("**" + "---> Operador Aritmético ---> " + numLinea + " 30");
                        i++;
                    } else
                    {
                        System.out.println(aux + "---> Operador Aritmético ---> " + numLinea + " 30");
                    }
                }
                break;
            case '%':
                System.out.println(letra + "---> Operador Aritmético ---> " + numLinea + " 30");
                break;
            case '=':
                if (linea.length() - 1 == indice)
                {
                    System.out.println(aux + "---> Operador de Asignación ---> " + numLinea + " 32");
                } else
                {
                    if (linea.charAt(indice + 1) == '=')
                    {
                        System.out.println("==" + "---> Operador de Comparación ---> " + numLinea + " 31");
                        i++;
                    } else
                    {
                        System.out.println(aux + "---> Operador de Asignación ---> " + numLinea + " 30");
                    }
                }
                break;
            case '<':
                if (linea.length() - 1 == indice)
                {
                    System.out.println(aux + "---> Operador de Comparación ---> " + numLinea + " 31");
                } else
                {
                    if (linea.charAt(indice + 1) == '=')
                    {
                        System.out.println("<=" + "---> Operador de Comparación ---> " + numLinea + " 31");
                        i++;
                    } else
                    {
                        System.out.println(aux + "---> Operador de Comparación ---> " + numLinea + " 31");
                    }
                }
                break;
            case '>':
                if (linea.length() - 1 == indice)
                {
                    System.out.println(aux + "---> Operador de Comparación ---> " + numLinea + " 31");
                } else
                {
                    if (linea.charAt(indice + 1) == '=')
                    {
                        System.out.println(">=" + "---> Operador de Comparación ---> " + numLinea + " 31");
                        i++;
                    } else
                    {
                        System.out.println(aux + "---> Operador de Comparación ---> " + numLinea + " 31");
                    }
                }
                break;
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                if (indice != linea.length() - 1 || indice == linea.length() - 1)
                {
                    buffer = new StringBuffer();
                    boolean b = true;
                    while (esNumero(aux) && b)
                    {
                        buffer.append(aux);
                        if (indice >= linea.length() - 1)
                        {
                            b = false;
                        } else
                        {
                            indice++;
                            aux = linea.charAt(indice);
                            letra = aux;
                            i = indice - 1;
                        }
                    }
                    if (buffer.toString().contains("."))
                    {
                        Pattern enteros = Pattern.compile("^[1-9]\\d*(\\.\\d+)?$");
                        Matcher m = enteros.matcher(buffer.toString());
                        if (m.find())
                        {
                            System.out.println(buffer.toString() + "---> Número flotante ---> " + numLinea + " 38");
                        } else
                        {
                            System.out.println("Número flotante no válido, en línea " + numLinea);
                            System.exit(1);
                        }
                    } else
                    {
                        Pattern enteros = Pattern.compile("\\d*");
                        Matcher m = enteros.matcher(buffer.toString());
                        if (m.find())
                        {
                            System.out.println(buffer.toString() + "---> Número entero ---> " + numLinea + " 37");
                        } else
                        {
                            System.out.println("Número entero no válido, en línea " + numLinea);
                            System.exit(1);
                        }
                    }
                }
                break;
            case '"':
                int cont = 0;
                int j = 0;
                buffer = new StringBuffer();
                for (j = 0; j < linea.length() && cont < 2; j++)
                {
                    if (linea.charAt(j) != '"')
                    {
                        buffer.append(linea.charAt(j));
                    } else
                    {
                        buffer.append('"');
                        cont++;
                    }
                }
                if (linea.charAt(buffer.toString().length() - 1) == '"')
                {
                    System.out.println(buffer.toString() + "---> Cadena ---> " + numLinea + " 40");
                }
                i = j - 1;
                break;
            default:

        }
    }

    private String transicion(String estado, char entrada)
    {
        int posicionEstado = -1;
        int posicionEntrada = -1;
        boolean b = false;

        for (int estados = 0; estados < tablaTransiciones.length && !b; estados++)
        {
            if (tablaTransiciones[estados][0].equals(estado))
            {
                posicionEstado = estados;
                b = true;
            }
        }

        b = false;

        for (int entradas = 0; entradas < tablaTransiciones.length && !b; entradas++)
        {
            if (tablaTransiciones[0][entradas].equals(Character.toString(entrada)))
            {
                posicionEntrada = entradas;
                b = true;
            }
        }

        return tablaTransiciones[posicionEstado][posicionEntrada];
    }

    private boolean esEspacioEnBlanco(char c)
    {
        switch (c)
        {
            case ' ':
            case '\t':
            case '\n':
            case '\f':
                return true;
        }
        return false;
    }

    private boolean esNumero(char c)
    {
        return (c >= '0' && c <= '9' || c == '.');
    }

    private boolean esInicioDeIdentificador(char c)
    {
        return (c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' || c == '_' || c == '$');
    }

    private boolean isIdentifierPart(char c)
    {
        return (esInicioDeIdentificador(c) || esNumero(c));
    }

}
