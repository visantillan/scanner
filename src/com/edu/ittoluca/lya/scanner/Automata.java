/*
 * Copyright (C) 2015 Víctor <viktor.santillan@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.edu.ittoluca.lya.scanner;

import com.edu.ittoluca.lya.archivos.ManejaArchivo;

/**
 *
 * @author Víctor <viktor.santillan@gmail.com>
 */
public class Automata
{

    private ManejaArchivo archivo;
    private String[][] tablaTransiciones;
    private String entrada;

    public Automata(String entrada)
    {
        archivo = new ManejaArchivo();
        tablaTransiciones = archivo.copiarAMatrix();
    }

    public void aceptaEntrada(String entrada)
    {
        this.entrada = entrada;
        int tamanioEntrada = entrada.length();

        

    }
    
    private String transicion(String estado, char entrada)
    {
        int posicionEstado = -1;
        int posicionEntrada = -1;
        boolean b = false;

        for (int estados = 0; estados < tablaTransiciones.length && !b; estados++)
        {
            if (tablaTransiciones[estados][0].equals(estado))
            {
                posicionEstado = estados;
                b = true;
            }
        }

        b = false;

        for (int entradas = 0; entradas < tablaTransiciones.length && !b; entradas++)
        {
            if (tablaTransiciones[0][entradas].equals(Character.toString(entrada)))
            {
                posicionEntrada = entradas;
                b = true;
            }
        }

        return tablaTransiciones[posicionEstado][posicionEntrada];
    }

   

}
