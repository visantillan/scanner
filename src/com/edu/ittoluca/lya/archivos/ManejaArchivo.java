/*
 * Copyright (C) 2015 Víctor <viktor.santillan@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */package com.edu.ittoluca.lya.archivos;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Víctor <viktor.santillan@gmail.com>
 */
public class ManejaArchivo
{

    private List<String> tablaTransicion;
    private List<String> codigo;
    private final static String nombreArchivo = "tabla.txt";

    public ManejaArchivo()
    {
        try
        {
            Path rutaTabla = FileSystems.getDefault().getPath(".", this.nombreArchivo);
            tablaTransicion = Files.readAllLines(rutaTabla, Charset.defaultCharset());
            
            Path rutaCodigo = FileSystems.getDefault().getPath(".", "codigo.atx");
            codigo = Files.readAllLines(rutaCodigo, Charset.defaultCharset());
        } catch (IOException ex)
        {
            ex.printStackTrace(System.out);
        }
    }

    public String[][] copiarAMatrix()
    {
        int contador = 0;
        String[] fila;

        if (tablaTransicion.isEmpty())
        {
            return null;
        }

        String[][] matrizTransicion = new String[getNumeroFilas()][getNumeroColumnas()];

        for (String linea : tablaTransicion)
        {
            for (int i = 0; i < linea.split("\\s+").length; i++)
            {
                matrizTransicion[contador][i] = linea.split("\\s+")[i];
            }
            contador++;
        }

        return matrizTransicion;
    }

    public List<String> getTablaTransicion()
    {
        return tablaTransicion;
    }
    
    public List<String> getCodigo()
    {
        return codigo;
    }

    public int getNumeroFilas()
    {
        if (tablaTransicion.isEmpty())
        {
            return 0;
        }

        tablaTransicion.removeAll(Collections.singleton(""));
        return tablaTransicion.size();
    }

    public int getNumeroColumnas()
    {
        if (tablaTransicion.isEmpty())
        {
            return 0;
        }

        return tablaTransicion.get(0).split("\\s+").length;
    }
}
